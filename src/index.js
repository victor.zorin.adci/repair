import '@babel/polyfill';
import './scss/main.scss';
import {Swiper, Controller, Pagination, Navigation} from 'swiper';
Swiper.use([Controller, Pagination, Navigation]);

//fix CustomEvent for IE11 
(function () {
    if ( typeof window.CustomEvent === "function" ) return false;
  
    function CustomEvent ( event, params ) {
      params = params || { bubbles: false, cancelable: false, detail: undefined };
      const evt = document.createEvent( 'CustomEvent' );
      evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
      return evt;
     }
    CustomEvent.prototype = window.Event.prototype;
  
    window.CustomEvent = CustomEvent;
  })();

// slider

let menu =  ['Rostov-on-Don, Admiral', 'Sochi Thieves', 'Rostov-on-Don Patriotic'];

let sliderProjectsImages = new Swiper('.slider-img', {
    navigation: {
        nextEl: '.image-slider-arrow-r',
        prevEl: '.image-slider-arrow-l'
    },
    pagination: {
        el: '.swiper-pagination-img',
        clickable: true,
        renderBullet: function (index, className) {
            return '<button class="' + className + '">' +  (menu[index]) + '</button>';
          },
    },
    slidesPerView: 1,
    spaceBetween: 20,
    loop: true,
    breakpoints: {
        720: {
          slidesPerView: 1.2,
          spaceBetween: 30
        },
      }
});

let sliderProjectsData = new Swiper('.slider-data', {
    navigation: {
        nextEl: '.slider__arrow-r',
        prevEl: '.slider__arrow-l'
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<button class="' + className + '">' + '</button>';
          },
    },
    slidesPerView: 1,
    spaceBetween: 20,
    loop: true,
});


//video
const video = document.getElementById('control-video');
const overlay = document.getElementById('overlay');

const play = () => {
    if (video.paused) {
        video.play();
        video.setAttribute('controls', 'controls');
        overlay.className = "control__overlay--play";
    } else {
        video.pause();
    }
};

overlay.addEventListener("click", play);


// menu
const burger = document.querySelector('.burger');
const nav = document.querySelector('.burger-menu-list');
const navLinks = document.querySelectorAll('.burger-menu-list li');

const toggleNav = () => {
    burger.addEventListener('click', ()=> {
        nav.classList.toggle('nav-active');
        // navLinks.forEach((link, index) => 
        for (const link of navLinks) {
            if (link.style.animation) {
                link.style.animation = '';
            } else {
                link.style.animation =`navLinkFade 0.5 ease forwards 2s`
    }};
        burger.classList.toggle('toggle')  
    });
};

toggleNav();

// scroll

const scroll = document.querySelector('.hero__scroll-btn');

scroll.addEventListener('click', () => {
    // window.scrollBy(0, 800);
    window.scrollTo({
        top: 800,
        left: 0,
        behavior: 'smooth'
    });
});
